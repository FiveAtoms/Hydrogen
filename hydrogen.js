class Requests {
	post = (url, params) => {
		return new Promise((resolve, reject) => {
			const r = new XMLHttpRequest();

			r.open('POST', url, true);
			r.setRequestHeader('Content-Type', 'application/json');

			r.onload = () => {
				r.readyState == 4 && r.status === 200 ? resolve(JSON.parse(r.response)) : reject(r.response);
			};

			r.send(JSON.stringify(params));
		});
	};

	get = (url, json = true) => {
		return new Promise((resolve, reject) => {
			const r = new XMLHttpRequest();
			r.open('GET', url, true);

			r.onload = () => {
				if (r.readyState == 4 && r.status === 200) {
					json ? resolve(JSON.parse(r.response)) : resolve(r.response);
				} else {
					reject(r.response);
				}
			};

			r.send(null);
		});
	};
}

// DOM
const loop_add_class = (e, classes) => {
	for (let index = 0; index < classes.length; index++) {
		e.classList.add(classes[index]);
	}
};

const loop_remove_class = (e, classes) => {
	for (let index = 0; index < classes.length; index++) {
		e.classList.remove(classes[index]);
	}
};

// Main
class Hydrogen extends Function {
	constructor() {
		super('return arguments.callee._call.apply(arguments.callee, arguments)');

		this.req = new Requests();

		this.node = null;
	}

	get_node = () => {
		console.log(this.node);
	};

	// Classes
	remove_class = (v) => {
		typeof v === 'string' ? this.node.classList.remove(v) : loop_remove_class(this.node, v);
	};
	add_class = (v) => {
		typeof v === 'string' ? this.node.classList.add(v) : loop_add_class(this.node, v);
	};

	// Attributes
	attr = (...args) => {
		if (args.length === 1) {
			return this.node.getAttribute(args[0]);
		} else {
			this.node.setAttribute(args[0], args[1]);
		}
	};

	each = (e, callback) => {
		document.querySelectorAll(e).forEach((e) => {
			callback(e);
		});
	};

	_call(...args) {
		this.node = document.querySelector(args[0]);
	}
}


const $ = new Hydrogen();