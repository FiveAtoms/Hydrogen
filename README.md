# Hydrogen

## Get started

Hydrogen is instantiated by default at "$."

### Set a node

You need to set an active node to use Hydrogen. You do so by calling the main object.

The parameter is a single string that accepts normal `querySelector` strings.

Example for `body` element:

```
$("body");
```

### Running a method on a node

After you've set a node like above, you can use it indefinitely until you set a new node.

To run a method

```
$.add_class();
```

If you attempt to reset a node *and* make a method call, you'll get an error:

```
$("body").add_class(); // Don't do this
```

## Requests

Version 0.0.2 introduced requests. You can use a request method by running it from the `req` object within Hydrogen. To see these in action, visit the `examples/requests/` folder.

## Methods

- Methods:
  - `get_node`:
    - does:
      - logs current node to console
  - `add_class`:
    - params:
      - `str` OR `array`
    - does:
      - If `str`, will add a single class;
      - if `array`, will recursively add classes
  - `remove_class`
    - params:
      - `str` OR `array`
    - does:
      - If `str`, will add a single class;
      - if `array`, will recursively add classes
  - `attr`
    - params:
      - 1 OR 2 `strings`
    - does:
      - If `1` string, will return attribute specified;
      - If `2` strings, will set attribute to second argument.
  - `each`
    - params:
      - `str`
      - `func`
    - does:
      - Accepts a selector string that then loops over with the callback
- Objects:
  - `node`
    - The current node. If you attempt to set it to a non-existing element, it will return `null`
  - `req`
    - `req.get`
      - params:
        - url : `str`
        - json : `bool`
      - does:
        - Performs a GET request to the specified URL.
    - `req.post`
      - params:
        - url : `str`
        - params : `obj`
      - does:
        - Performs a POST request to the specified URL with the specified params.