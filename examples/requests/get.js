const promise = $.req.get('http://localhost:3000/api/hello', true);

const resolve = (v) => {
    console.log(v.name);
};

const reject = (v) => {
    console.log(v);
};

promise.then(resolve, reject);