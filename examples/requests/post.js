const params = {
    name: 'arepo',
};

const promise = $.req.post('http://localhost:3000/api/hello', params);

const resolve = (v) => {
    console.log(v.name);
};

const reject = (v) => {
    console.log(v);
};

promise.then(resolve, reject);